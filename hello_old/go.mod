module github.com/cloudwego/kitex-examples/hello_old

go 1.19

require git.apache.org/thrift.git v0.10.0

replace git.apache.org/thrift.git v0.10.0 => ../third_party/thrift
