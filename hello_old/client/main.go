package main

import (
	"log"
	"time"

	"git.apache.org/thrift.git/lib/go/thrift"
	"github.com/cloudwego/kitex-examples/hello_old/gen-go/api"
)

func main() {
	address := ":8888"
	tSocket, err := thrift.NewTSocketTimeout(address, time.Second)
	if err != nil {
		log.Fatal("new socket fail, err:", err)
	}
	log.Println("new socket success")

	transportFactory := thrift.NewTBufferedTransportFactory(10240)
	transport := transportFactory.GetTransport(tSocket)
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()
	err = transport.Open()
	if err != nil {
		log.Fatal("conenct fail")
	}
	log.Println("connect success")

	client := api.NewHelloClientFactory(transport, protocolFactory)
	for {
		req := &api.Request{Message: "my request"}
		resp, err := client.Echo(req)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(resp)
		time.Sleep(time.Second)
	}
}
