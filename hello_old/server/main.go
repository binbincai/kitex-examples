package main

import (
	"fmt"
	"log"

	"git.apache.org/thrift.git/lib/go/thrift"
	"github.com/cloudwego/kitex-examples/hello_old/gen-go/api"
)

func main() {
	addr := ":8888"
	transport, err := thrift.NewTServerSocket(addr)
	if err != nil {
		log.Fatal(err)
	}

	transportFactory := thrift.NewTBufferedTransportFactory(2048)
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()

	processor := api.NewHelloProcessor(&Handler{})
	server := thrift.NewTSimpleServer4(processor, transport, transportFactory, protocolFactory)

	fmt.Println("test001")
	fmt.Println("test002")
	fmt.Println("test003")
	fmt.Println("test004")
	fmt.Println("test005")
	fmt.Println("test006")
	fmt.Println("test007")
	fmt.Println("test008")
	fmt.Println("test009")
	fmt.Println("test010")
	fmt.Println("test011")
	fmt.Println("test012")
	fmt.Println("test013")

	log.Println("start server... on ", addr)
	panic(server.Serve())
}

type Handler struct {
}

func (h *Handler) Echo(req *api.Request) (r *api.Response, _ error) {
	r = &api.Response{Message: req.Message}
	return
}

func (h *Handler) GetMonitor() api.HelloServerMonitor {
	return nil
}
